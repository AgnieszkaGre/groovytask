@Grapes([
        @Grab(group='org.apache.poi', module='poi', version='3.8'),
        @Grab(group='org.apache.poi', module='poi-ooxml', version='3.8')
])

​import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import org.apache.commons.io.FileUtils
import java.io.*



class GroovyExcelParser {

    def parse (path) {
        if (path == null) {
            System.out.println("Please provide file name.")
        } else {
            InputStream inp = new FileInputStream(path)
            Workbook wb = WorkbookFactory.create(inp);
            def files = []
            files = parseAllSheetsToHtml(wb)
            files
        }

    }

    def parseAllSheetsToHtml(Workbook workBook) {
        def files = []
        def sheetsConfigMap = getSheetsConfiguration(workBook);

        sheetsConfigMap.each { entry ->
            String sheetName
            String sheetRange
            sheetName = entry.key
            Sheet sheet = workBook.getSheet(sheetName)

            sheetRange = entry.value
            String[] range;
            range = sheetRange.split(':');

            CellReference crFirst = new CellReference(range[0]);
            Row rowFirst = sheet.getRow(crFirst.getRow());
            int rowFirstNumber = rowFirst.getRowNumber();
            Cell cellFirst = rowFirst.getCell(crFirst.getCol());
            int cellFirstNumber = cellFirst.getColumnIndex();

            CellReference crLast = new CellReference(range[1]);
            Row rowLast = sheet.getRow(crLast.getRow());
            int rowLastNumber = rowLast.getRowNumber();
            Cell cellLast = rowLast.getCell(crLast.getCol());
            int cellLastNumber = cellLast.getColumnIndex();

            File file = parseSheet(sheet, sheetName, rowFirstNumber, rowLastNumber, cellFirstNumber, cellLastNumber)
            files.add(file)

        }
        files
    }



    def getSheetsConfiguration(workBook) {
        confSheet = workBook.getSheetAt(0);
        def sheetsMap = [:]

        for (Row row : confSheet) {
            Cell cellSheetName = row.getCell(0, Row.RETURN_BLANK_AS_NULL);
            String sheetName = getValue(row, cellSheetName)
            Cell cellSheetRange = row.getCell(1, Row.RETURN_BLANK_AS_NULL);
            String sheetRange = getValue(row, cellSheetRange)
            sheetsMap.put(sheetName, sheetRange)
        }
        sheetsMap
    }



    def parseSheet(sheet, sheetName, rowFirst, rowLast, columnFirst, columnLast) {

        StringBuffer outputHtml = new String();
        outputHtml = getFileContent(sheet, sheetName, rowFirst, rowLast, columnFirst, columnLast);
        String htmlFileName = new String();
        htmlFileName = getHtmlFileName(sheetName);
        File htmlFile = createHtmlFile(htmlFileName, outputHtml);
        htmlFile
    }

    def getHtmlFileName(String sheetName) {
        sheetName = sheetName.replaceAll(" ", "_").toLowerCase();
        String filename = sheetName + ".html";
        filename
    }

    def getFileContent(Sheet sheet, String sheetName, int firstRow, int lastRow, int firstColumn, int lastColumn) {
        StringBuffer output
        output.append("<html><head></head><body><p>" + sheetName + "</p><hr>")

        for (int rowNum = firstRow; rowNum <= lastRow; rowNum++) {
            StringBuffer rowValue
            Row row = sheet.getRow(rowNum);
            StringBuffer htmlCellValue
            String cellValue

            for (int cellNum = firstColumn; cellNum <= lastColumn; cellNum++) {
                Cell cell = row.getCell(cellNum, Row.RETURN_BLANK_AS_NULL);
                if (cellNum > firstColumn) {
                    htmlCellValue.append(",")
                }
                cellValue = getValue(row, cell)
                htmlCellValue.append(cellValue)
                rowValue.append(htmlCellValue)

            }
            rowValue.append("<br>")
            output.append(rowValue)
        }
        output.append("</body></html>")
        output
    }


    def getValue(Row row, Cell cell) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        def value = ""
        String htmlInput

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString();
                htmlInput = "<a href='" + value + "'>" + value + "</a>"
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    htmlInput = cell.getDateCellValue();
                } else {
                    htmlInput = cell.getNumericCellValue();
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                htmlInput = cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                htmlInput = cell.getCellFormula();
                break;
            default:
                htmlInput = ""
        }
        htmlInput
    }

    def createHtmlFile(htmlFileName, htmlString) {
        File newHtmlFile = new File(htmlFileName);
        FileUtils.writeStringToFile(newHtmlFile, htmlString);
        newHtmlFile
    }


    public static void main(String[]args) {
        def filename = 'input.xlsx'
        GroovyExcelParser parser = new GroovyExcelParser()
        def fileList = parser.parse(filename)
        System.out.println(fileList)

    }
}